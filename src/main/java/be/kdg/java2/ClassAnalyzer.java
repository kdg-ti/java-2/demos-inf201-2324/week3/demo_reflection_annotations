package be.kdg.java2;

import be.kdg.java2.annotations.HeavyMethod;

import java.lang.annotation.Annotation;
import java.lang.reflect.*;

public class ClassAnalyzer {
    private Class clazz;

    public ClassAnalyzer(Class clazz) {
        this.clazz = clazz;
    }

    public void analyse() {
        //TODO:
        System.out.println("Analysing class " + clazz.getName());
        // - check of klasse een default constructor heeft...
        System.out.println("Constructors of this class:");
        Constructor[] constructors = clazz.getDeclaredConstructors();
        boolean containsDefaultConstructor = false;
        for (Constructor constructor : constructors) {
            System.out.println(constructor);
            if (constructor.getParameterCount() == 0) {
                containsDefaultConstructor = true;
            }
        }
        if (!containsDefaultConstructor) {
            System.out.println("WARNING: NO DEFAULT CONSTRUCTOR FOUND!");
        }
        // - check of alle attributen private zijn
        Field[] fields = clazz.getDeclaredFields();
        System.out.println("All attributes of the class " + clazz.getSimpleName());
        boolean allFieldsArePrivate = true;
        for (Field field : fields) {
            System.out.println(field);
            if (!Modifier.isPrivate(field.getModifiers())) {
                allFieldsArePrivate = false;
            }
        }
        if (!allFieldsArePrivate) {
            System.out.println("WARNING: NOT ALL ATTRIBUTES ARE PRIVATE!");
        }
        // - check of er een getter is voor elk attribuut
        Method[] methods = clazz.getDeclaredMethods();
        System.out.println("All methods of the class " + clazz.getSimpleName());
        boolean hasToStringMethod = false;
        for (Method method : methods) {
            System.out.println(method.getName());
            if (method.getName().equals("toString")
                    && method.getReturnType().equals(String.class)
                    && method.getParameterCount() == 0
                    && Modifier.isPublic(method.getModifiers())) {
                hasToStringMethod = true;
            }
        }
        //TODO: werk dit verder uit:
        //  - overloop alle attributen
        //  - maak een String als volgt: "get" + naam van attribuut met hoofdletter
        //  - check of die String voorkomt in de lijst van alle methodenamen

        // - check of er een toString voorzien is
        if (!hasToStringMethod) {
            System.out.println("WARNING: NO TOSTRING METHOD FOUND!");
        }
        // - check of je een default object kan aanmaken van deze klasse en er de getters op kan uitvoeren
        try {
            Constructor constructor = clazz.getDeclaredConstructor();
            Object object = constructor.newInstance();
            System.out.println("Created default object:" + object.toString());
            System.out.println("Running all getters:");
            for (Method method: methods) {
                if (method.getName().startsWith("get")) {
                    System.out.println("Result of running " + method.getName() + " on " + object.toString() + ":"
                            + method.invoke(object));
                }
            }
        } catch (NoSuchMethodException | InvocationTargetException | InstantiationException | IllegalAccessException e) {
            System.out.println("WARNING: UNABLE TO CREATE DEFAULT OBJECT OF THIS CLASS:");
            System.out.println(e.getMessage());
            //throw new RuntimeException(e);
        }
        //check of er @HeavyMethod methods zijn, en run die eens en time ze...
        for (Method method : methods) {
            Annotation[] annotations = method.getDeclaredAnnotations();
            if (annotations.length>0) {
                for (Annotation annotation : annotations) {
                    if (annotation.annotationType().equals(HeavyMethod.class)) {
                        System.out.println("Heavy method found!");
                        Constructor constructor = null;
                        try {
                            constructor = clazz.getDeclaredConstructor();
                            Object object = constructor.newInstance();
                            long timeBefore = System.currentTimeMillis();
                            method.invoke(object);
                            long duration = System.currentTimeMillis() - timeBefore;
                            System.out.println("Method took " + duration + " milliseconds...");
                            int maxtime = ((HeavyMethod) annotation).maxtime();
                            if (duration>maxtime) {
                                System.out.println("WARNING: HEAVY METHOD TOOK MORE THAN " + maxtime + " MILLISECONDS!");
                            }
                        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException |
                                 InstantiationException e) {
                            throw new RuntimeException(e);
                        }
                    }
                }
            }
        }
    }
}
