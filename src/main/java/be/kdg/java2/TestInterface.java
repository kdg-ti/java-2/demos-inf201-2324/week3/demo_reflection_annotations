package be.kdg.java2;

@FunctionalInterface
public interface TestInterface {
    void run();
}
