package be.kdg.java2;

import be.kdg.java2.annotations.HeavyMethod;

import java.time.LocalDate;
import java.util.Random;

public class Student {
    private String name;
    int number;
    private LocalDate birthday;

    public Student() {
        this.name = "<anoniem>";
        this.number = -1;
        this.birthday = null;
    }

    public Student(String name, int number, LocalDate birthday) {
        this.name = name;
        this.number = number;
        this.birthday = birthday;
    }

    public String getName() {
        return name;
    }

    public int getNumber() {
        return number;
    }

    @HeavyMethod(maxtime=5000)
    public int calculateLeerkrediet(){
        //stel dit duurt misschien vrij lang...
        try {
            Thread.sleep(new Random().nextInt(10000));
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
        return 123;
    }

    @Deprecated
    public LocalDate getBirthday() {
        return birthday;
    }

    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", number=" + number +
                ", birthday=" + birthday +
                '}';
    }
}
