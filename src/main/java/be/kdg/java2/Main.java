package be.kdg.java2;

public class Main {
    public static void main(String[] args) {
        System.out.println("Demo Reflection Annotations");

        ClassAnalyzer classAnalyzer
                = new ClassAnalyzer(Student.class);
        classAnalyzer.analyse();
    }
}
